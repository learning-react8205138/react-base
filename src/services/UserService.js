import axios from "./axios.js";


const fetchOrInsert = function (currPage) {
    return axios.get(`/api/users?page=${currPage}`);
}

export { fetchOrInsert };