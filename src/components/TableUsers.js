import { Table } from "react-bootstrap";
import { fetchOrInsert } from "../services/UserService";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";

function TableUsers(props) {

    const [users, setUsers] = useState([]);
    const [totalUsers, setTotalUsers] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [currPage, setCurrPage] = useState(1);


    const getUsers = async function (currPage) {
        const res = await fetchOrInsert(currPage);

        if (res && res.data) {
            setTotalUsers(res.total);
            setTotalPages(res.total_pages);
            setUsers(res.data);
        }
    }

    useEffect(() => {
        getUsers(currPage);
    }, [currPage]);

    const handlePageClick = (event) => {
        const page = +event.selected + 1;
        setCurrPage(page);
    }

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mail</th>
                    </tr>
                </thead>
                <tbody>
                    {users && users.length > 0 &&
                        users.map((user, index) => {
                            return (
                                <tr key={`users-${index}`}>
                                    <td>{user.id}</td>
                                    <td>{user.first_name}</td>
                                    <td>{user.last_name}</td>
                                    <td>{user.email}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>

            <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={3}
                pageCount={totalPages}
                previousLabel="< previous"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
        </>
    );
}

export default TableUsers;